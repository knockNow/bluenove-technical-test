# The technical test for Bluenove completed.


Do not hesitate if you have questions about my choices of the technical stack.

### Installation

This project requires [Node.js](https://nodejs.org/) v6+ to run.

Install the dependencies and devDependencies and start the server.

```sh
$ npm i
$ npm start
```

### Dependencies

* React
* Redux
* Saga (Redux-saga)
* Styled-components
* Webpack
* React-loadable
