import React from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'

const SUCCESS = '#418a41'
const ERROR = '#ff4141'

const Form = styled.form`
  width: 50%;
  padding: 1em;
  display: flex;
  flex-direction: column;

  input {
    margin: 1em;
    outline: none;
    color: #2b8bf4;
    font-size: 1.2em;
    font-weight: 600;
    padding: 0.6em 1em;
    margin-bottom: 0.5em;
    transition: all 200ms;
    border: 2px solid #e6e6e6;
    border-radius: 10px 10px 0px 0px;

    &:focus {
      padding-left: 1.5em;
    }
  }

  textarea {
    padding: 1em;
    margin: 0 1em;
    height: 140px;
    outline: none;
    color: #2b8bf4;
    border-radius: 0;
    font-size: 1.2em;
    transition: all 200ms;
    border: 2px solid #e6e6e6;

    &:focus {
      padding-left: 1.5em;
    }
  }

  button {
    border: 0;
    padding: 1em;
    outline: none;
    color: #2b8bf4;
    font-size: 1.2em;
    font-weight: 600;
    margin: 0.5em 1em;
    letter-spacing: 0px;
    transition: all 200ms;
    background-color: #e1e1e1;
    border-radius: 0px 0px 10px 10px;

    &:hover {
      cursor: pointer;
      letter-spacing: 2px;
    }
  }
`;

const Title = styled.h3`
  color: #2b8bf4;
  text-align: center;
`;

const Label = styled.label`
  color: #fff;
  margin: 1em 3em;
  text-align: left;
  padding: 1em 2em;
  font-weight: 600;
  border-radius: 50px;
  background-color: ${({state}) => state};
`;

class CreateMessage extends React.Component {
  constructor(props) {
    super(props)
    this.state = { form : {} }
    this.handleChange = this.handleChange.bind(this)
    this.sendForm = this.sendForm.bind(this)
  }

  handleChange({currentTarget}) {
    const { name, value } = currentTarget
    const { form } = this.state
    this.setState({ form : { ...form, [name]: value } })
  }

  sendForm(evt) {
    evt.preventDefault()
    const { sendMessage } = this.props
    const { form } = this.state
    sendMessage(form)
  }

  render() {
    const { message_err, message_succ } = this.props
    return (
      <Form action="">
        <Title>Send a new Message</Title>
        <input
          name="title"
          id="title"
          type="text"
          placeholder="Title"
          onChange={this.handleChange} />
        <textarea
          name="body"
          id="body"
          placeholder="message..."
          onChange={this.handleChange}></textarea>
        <button type="submit" onClick={this.sendForm}>send</button>
        {message_err && <Label state={ERROR}>{message_err}</Label>}
        {message_succ && <Label state={SUCCESS}>{message_succ}</Label>}
      </Form>
    )
  }
}

const mapStateToProps = (state) => ({
  message_err: state.app.message_err || '',
  message_succ: state.app.message_succ || '',
})

export default connect(
  mapStateToProps,
)(CreateMessage)
