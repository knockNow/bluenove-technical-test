import React from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'

import Message from 'Components/Message/Loadable'

const Container = styled.div`
  width: 50%;
  padding: 1em;
  display: flex;
  flex-direction: column;
`;

// Step 1 : Take id of Message
// Step 2 : Sort by id
// Step 3 : map on this and render
const MapMessage = ({messages}) =>
  Object.keys(messages).map(x => messages[x].id).sort((a, b) => b - a)
    .map((key, index) =>
      messages[key] !== undefined &&
        <Message
          key={index}
          title={messages[key].title}>
            {messages[key].body}
        </Message>)

export class ListMessage extends React.Component {
  componentWillMount() {
    const { messages, loadMessage } = this.props
    if (Object.keys(messages).length === 0) loadMessage()
  }

  render() {
    const { messages } = this.props
    return (
      <Container>
        <MapMessage messages={messages} />
      </Container>
    )
  }
}

const mapStateToProps = (state) => ({
  messages: state.app.message || {},
})

export default connect(
  mapStateToProps,
)(ListMessage)
