import React from 'react'
import styled, { keyframes } from 'styled-components'

const LoadingAnim = keyframes`
  100% {
    transform: rotate(360deg)
  }
`

const Container = styled.div`
  color: #fff;
  padding: 1em;
  margin: -8px;
  height: 100vh;
  display: flex;
  align-items: center;
  flex-direction: column;
  font-family: monospace;
  justify-content: center;
`

const Box = styled.div`
  width: 20px;
  height: 20px;
  padding: 2em;
  border-radius: 50%;
  position: relative;
  border: 5px solid #2b8bf4;
  animation: ${LoadingAnim} 2s linear infinite;
  &::after {
    left: -7px;
    content: '';
    width: 50px;
    height: 20px;
    display: block;
    position: absolute;
    background-color: #f8f7fc;
  }
`;

class Loading extends React.Component {
  render () {
    return (
      <Container>
        <Box />
      </Container>
    )
  }
}

export default Loading
