import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  color: #fff;
  padding: 1.7em;
  margin-bottom: 1em;
  transition: all 200ms;
  background-color: #2b8bf4;
  border-radius: 3px 20px 20px 20px;

  &:hover {
    cursor: pointer;
    margin-left: 1em;
    background-color: #0770e3;
  }

  h3 {
    margin-top: 0;
  }
`;

export class Message extends React.Component {
  render() {
    const { title, children } = this.props
    return (
      <Container>
        <h3>{title}</h3>
        <p>{children}</p>
      </Container>
    )
  }
}

export default Message
