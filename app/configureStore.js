import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import createReducer from './reducers'

import messageSaga from 'Containers/HomePage/saga'

export default function configureStore(initialState = {}, history) {

  const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : compose

  const sagaMiddleware = createSagaMiddleware()

  const enhancers = composeEnhancers(
    applyMiddleware(sagaMiddleware),
  )

  const store = createStore(
    createReducer(),
    initialState,
    enhancers,
  )

  sagaMiddleware.run(messageSaga)

  return store
}
