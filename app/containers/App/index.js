import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import styled from 'styled-components'

import HomePage from 'Containers/HomePage/Loadable'
import LoadingIndicator from 'Components/LoadingIndicator'

const Container = styled.div`
  font-size: 14px;
  font-family: 'Helvetica';
`;

class App extends React.Component {
  render() {
    return (
    <Router>
      <Container>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/loader" component={LoadingIndicator} />
      </Container>
    </Router>
    )
  }
}

export default App
