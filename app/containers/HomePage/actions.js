import {
  MESSAGE_LOAD,
  MESSAGE_LOAD_SUCCESS,
  MESSAGE_LOAD_ERROR,
  MESSAGE_SEND,
  MESSAGE_SEND_SUCCESS,
  MESSAGE_SEND_ERROR,
} from './constants'


// Load Message Action
export function loadMessageAction() {
  return {
    type: MESSAGE_LOAD,
    payload: {},
  }
}

export function messagesLoaded(payload) {
  return {
    type: MESSAGE_LOAD_SUCCESS,
    payload,
  }
}

export function messagesLoadingError(payload) {
  return {
    type: MESSAGE_LOAD_ERROR,
    payload,
  }
}

// Send Message Action

export function sendMessageAction(payload) {
  return {
    type: MESSAGE_SEND,
    payload,
  }
}

export function messageSended(payload) {
  return {
    type: MESSAGE_SEND_SUCCESS,
    payload,
  }
}

export function messageSendingError(payload) {
  return {
    type: MESSAGE_SEND_ERROR,
    payload,
  }
}
