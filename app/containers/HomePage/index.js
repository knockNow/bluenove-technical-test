import React from 'react'
import PropTypes from 'prop-types';
import styled from 'styled-components'
import { connect } from 'react-redux'

import {
  loadMessageAction,
  sendMessageAction,
} from './actions'

import ListMessage from 'Components/ListMessage/Loadable'
import CreateMessage from 'Components/CreateMessage/Loadable'

const Container = styled.div`
  display: flex;
  padding: 4em 30em;
  align-items: center;
  flex-direction: column;
`;

const Title = styled.h3`
  color: #979797;
  text-align: center;
`;

const Wrapper = styled.div`
  display: flex;
`;

function HomePage({
  messages,
  onLoadMessage,
  onSendMessage,
}) {
  return (
    <Container>
      <Title>Bluenove Messenger - Technical test - Helie-Joly William (6h of work)</Title>
      <Wrapper>
        <ListMessage loadMessage={onLoadMessage} />
        <CreateMessage sendMessage={onSendMessage} />
      </Wrapper>
    </Container>
  )
}

const mapDispatchToProps = (dispatch) => ({
  onLoadMessage: () => dispatch(loadMessageAction()),
  onSendMessage: core => dispatch(sendMessageAction(core)),
})

const mapStateToProps = (state) => ({
  messages: state.app.message || {},
  message_err: state.app.message_err || {},
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomePage)
