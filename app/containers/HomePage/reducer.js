import {
  MESSAGE_LOAD_SUCCESS,
  MESSAGE_LOAD_ERROR,
  MESSAGE_SEND_SUCCESS,
  MESSAGE_SEND_ERROR,
} from './constants'

function homePageReducer(state = {}, { type, payload }) {
  switch (type) {
    case MESSAGE_LOAD_SUCCESS:
      return {
        ...state,
        message: payload,
      }
    case MESSAGE_LOAD_ERROR:
      return {
        ...state,
        message_err: payload,
      }
    case MESSAGE_SEND_SUCCESS:
      return {
        ...state,
        message: [ ...state.message, payload],
        message_succ: 'Message sent with success.'
      }
    case MESSAGE_SEND_ERROR:
      return {
        ...state,
        message_err: 'Error : Message not sent. Try again.',
      }
    default:
      return state
  }
}

export default homePageReducer
