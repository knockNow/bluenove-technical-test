import { call, put, takeEvery } from 'redux-saga/effects'
import {
  MESSAGE_LOAD,
  MESSAGE_SEND,
} from 'Containers/HomePage/constants'
import {
  messagesLoaded,
  messagesLoadingError,
  messageSended,
  messageSendingError,
} from 'Containers/HomePage/actions'

import request from 'Utils/request'

function* fetchMessage() {
  const requestURL = 'https://jsonplaceholder.typicode.com/posts'

  try {
    const response = yield call(request, requestURL)
    yield put(messagesLoaded(response))
  } catch (err) {
    yield put(messagesLoadingError(err))
  }
}

function* sendMessage({payload}) {
  const requestURL = 'https://jsonplaceholder.typicode.com/posts'
  const options = {
    method: 'POST',
    headers: {
      "Content-type": "application/json; charset=UTF-8"
    },
    body: JSON.stringify(payload),
  }

  try {
    const response = yield call(request, requestURL, options)
    yield put(messageSended(response))
  } catch (err) {
    yield put(messageSendingError(err))
  }
}

function* messageSaga() {
  yield takeEvery(MESSAGE_LOAD, fetchMessage)
  yield takeEvery(MESSAGE_SEND, sendMessage)
}

export default messageSaga;
