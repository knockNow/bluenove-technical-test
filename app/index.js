import React from 'react'
import ReactDom from 'react-dom'
import { Provider } from 'react-redux'
import { Route } from 'react-router-dom'
import styled from 'styled-components'

import createStore from './configureStore'

import App from 'Containers/App'

const store = createStore()

const AppWrapper = styled.div`
  font-size: 14px;
  font-family: 'Helvetica';
`

class Render extends React.Component {
  render() {
    return (
      <AppWrapper>
        <Provider store={store}>
          <App />
        </Provider>
      </AppWrapper>
    )
  }
}

ReactDom.render(
  <Render />,
  document.getElementById('app')
)
