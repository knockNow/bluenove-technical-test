const path = require('path')

module.exports = {
  entry: './app/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  devServer: {
    contentBase: './dist',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          "babel-loader"
        ]
      }
    ]
  },
  resolve: {
    alias: {
      Components: path.resolve( __dirname, './app/components' ),
      Containers: path.resolve( __dirname, './app/containers' ),
      Utils: path.resolve( __dirname, './app/utils' ),
    },
    extensions: ['*', '.js', '.jsx'],
  },
}
